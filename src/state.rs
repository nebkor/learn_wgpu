use cgmath::{Deg, InnerSpace, Rotation3, Zero};
use wgpu::util::DeviceExt;
use winit::{dpi::PhysicalSize, event::WindowEvent, window::Window};

use crate::{
    camera::{Camera, CameraController, CameraUniform},
    model, texture, Quat, Vec3, INSTANCE_DISPLACEMENT, NUM_INSTANCES_PER_ROW,
};

pub struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    pub config: wgpu::SurfaceConfiguration,
    pub size: PhysicalSize<u32>,
    window: Window,
    clear_color: wgpu::Color,
    render_pipeline: wgpu::RenderPipeline,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    num_indices: u32,
    diffuse_bind_group: wgpu::BindGroup,
    _diffuse_texture: texture::Texture,
    camera: Camera,
    camera_uniform: CameraUniform,
    camera_buffer: wgpu::Buffer,
    camera_bind_group: wgpu::BindGroup,
    camera_controller: CameraController,
    instances: Vec<model::Instance>,
    instance_buffer: wgpu::Buffer,
    depth_texture: texture::Texture,
}

impl State {
    pub async fn new(window: Window) -> Self {
        let size = window.inner_size();

        // this is a handle to the GPU
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::VULKAN | wgpu::Backends::BROWSER_WEBGPU,
            dx12_shader_compiler: Default::default(),
        });

        // safety: we own the window and the surface can't outlive that, so we should be
        // good
        let surface = unsafe { instance.create_surface(&window) }.unwrap();

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptionsBase {
                power_preference: wgpu::PowerPreference::default(),
                force_fallback_adapter: false,
                compatible_surface: Some(&surface),
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: if cfg!(target_arch = "wasm32") {
                        wgpu::Limits::downlevel_webgl2_defaults()
                    } else {
                        wgpu::Limits::default()
                    },
                    label: None,
                },
                None,
            )
            .await
            .unwrap();

        let config = mk_surface_config(&surface, &adapter, &size);
        surface.configure(&device, &config);

        // textures
        let diffuse_bytes = include_bytes!("../happy-tree.png");
        let diffuse_texture =
            texture::Texture::from_bytes(&device, &queue, diffuse_bytes, "happy-tree texture")
                .unwrap();
        let (diffuse_bind_group, texture_bind_group_layout) =
            mk_texture_bindings(&device, &diffuse_texture);

        // geo
        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("vertex buffers"),
            contents: bytemuck::cast_slice(model::VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        });
        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("index buffs"),
            contents: bytemuck::cast_slice(model::INDICES),
            usage: wgpu::BufferUsages::INDEX,
        });

        // camera
        let camera = Camera::new(
            (0.0, 1.0, 2.0).into(),
            [0.0; 3].into(),
            crate::Vec3::unit_y(),
            config.width as f32 / config.height as f32,
            45.0,
            0.1,
            100.0,
        );

        let (camera_uniform, camera_buffer, camera_bind_group, camera_bind_group_layout) =
            mk_camera_shiz(&camera, &device);

        // render pipeline
        let render_pipeline = mk_render_pipeline(
            &device,
            &texture_bind_group_layout,
            &camera_bind_group_layout,
            config.format,
        );

        let depth_texture =
            texture::Texture::create_depth_texture(&device, &config, "depth_texture");

        let (instances, instance_buffer) = mk_instance_shiz(&device);

        Self {
            surface,
            device,
            queue,
            config,
            size,
            window,
            clear_color: wgpu::Color::default(),
            render_pipeline,
            vertex_buffer,
            index_buffer,
            num_indices: model::INDICES.len() as u32,
            diffuse_bind_group,
            _diffuse_texture: diffuse_texture,
            camera,
            camera_uniform,
            camera_buffer,
            camera_bind_group,
            camera_controller: CameraController::new(0.02),
            instances,
            instance_buffer,
            depth_texture,
        }
    }

    pub fn window(&self) -> &Window {
        &self.window
    }

    pub(super) fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        let (width, height) = (new_size.width, new_size.height);
        if width > 0 && height > 0 {
            self.size = new_size;
            self.config.width = width;
            self.config.height = height;
            self.depth_texture =
                texture::Texture::create_depth_texture(&self.device, &self.config, "depth_texture");
            self.surface.configure(&self.device, &self.config);
        }
    }

    pub(super) fn input(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::CursorMoved { position, .. } => {
                //
                let v = (self.size.height as f64 - position.y) / self.size.height as f64;
                let u = position.x / self.size.width as f64;
                self.clear_color = hv2color(u * 360.0, v);
                false
            }
            _ => self.camera_controller.process_events(event),
        }
    }

    pub(super) fn update(&mut self) {
        self.camera_controller.update_camera(&mut self.camera);
        self.camera_uniform.update_view_proj(&self.camera);
        self.queue.write_buffer(
            &self.camera_buffer,
            0,
            bytemuck::cast_slice(&[self.camera_uniform]),
        );
    }

    pub(super) fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        //
        let output = self.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("render encoder"),
            });

        {
            // we need this mutable borrow of encoder to end before we can call .finish()
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("render pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(self.clear_color),
                        store: true,
                    },
                })],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                    view: &self.depth_texture.view,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
            render_pass.set_bind_group(1, &self.camera_bind_group, &[]);
            // the '0' for the vertex buffer refers to the index of the description of the
            // buffer layout, in the render pipeline's `VertexState`'s "buffers"
            // parameter:
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
            render_pass.set_vertex_buffer(1, self.instance_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
            render_pass.draw_indexed(0..self.num_indices, 0, 0..self.instances.len() as _);
        }
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        Ok(())
    }
}

// helper fns
fn hv2color(h: f64, v: f64) -> wgpu::Color {
    // S is 1.0
    let chroma = v;
    let h_prime = h / 60.0;

    let x = chroma * (1.0 - ((h_prime % 2.0) - 1.0).abs());

    let (r, g, b) = match h_prime {
        _ if (0.0..1.0).contains(&h_prime) => (chroma, x, 0.0),
        _ if (1.0..2.0).contains(&h_prime) => (x, chroma, 0.0),
        _ if (2.0..3.0).contains(&h_prime) => (0.0, chroma, x),
        _ if (3.0..4.0).contains(&h_prime) => (0.0, x, chroma),
        _ if (4.0..5.0).contains(&h_prime) => (x, 0.0, chroma),
        _ if (5.0..6.0).contains(&h_prime) => (chroma, 0.0, x),
        _ => (chroma, x, 0.0),
    };

    wgpu::Color { r, g, b, a: 1.0 }
}

fn mk_instance_shiz(device: &wgpu::Device) -> (Vec<model::Instance>, wgpu::Buffer) {
    let instances = (0..NUM_INSTANCES_PER_ROW)
        .flat_map(|z| {
            (0..NUM_INSTANCES_PER_ROW).map(move |x| {
                let position = Vec3::new(x as f32, 0.0, z as f32) - INSTANCE_DISPLACEMENT;
                let rotation = if position.is_zero() {
                    Quat::from_angle_z(Deg(0.0))
                } else {
                    Quat::from_axis_angle(position.normalize(), Deg(45.0))
                };

                model::Instance { position, rotation }
            })
        })
        .collect::<Vec<_>>();
    let instance_data: Vec<model::InstanceRaw> = instances
        .iter()
        .copied()
        .map(<model::Instance as Into<model::InstanceRaw>>::into)
        .collect();
    let instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("instance buffs bish"),
        contents: bytemuck::cast_slice(&instance_data),
        usage: wgpu::BufferUsages::VERTEX,
    });
    (instances, instance_buffer)
}

fn mk_surface_config(
    surface: &wgpu::Surface,
    adapter: &wgpu::Adapter,
    size: &PhysicalSize<u32>,
) -> wgpu::SurfaceConfiguration {
    let caps = surface.get_capabilities(adapter);

    // shader code will assume sRGB images for textures
    let format = caps
        .formats
        .iter()
        .copied()
        .find(|f| f.describe().srgb)
        .unwrap_or(caps.formats[0]);

    wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format,
        width: size.width,
        height: size.height,
        present_mode: caps.present_modes[0],
        alpha_mode: caps.alpha_modes[0],
        view_formats: vec![],
    }
}

fn mk_camera_shiz(
    camera: &Camera,
    device: &wgpu::Device,
) -> (
    CameraUniform,
    wgpu::Buffer,
    wgpu::BindGroup,
    wgpu::BindGroupLayout,
) {
    let mut uniform = CameraUniform::new();
    uniform.update_view_proj(camera);

    let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Camera Buffer"),
        contents: bytemuck::cast_slice(&[uniform]),
        usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
    });

    let layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        entries: &[wgpu::BindGroupLayoutEntry {
            binding: 0,
            visibility: wgpu::ShaderStages::VERTEX,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }],
        label: Some("camera_bind_group_layout"),
    });

    let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
        layout: &layout,
        entries: &[wgpu::BindGroupEntry {
            binding: 0,
            resource: buffer.as_entire_binding(),
        }],
        label: Some("camera_bind_group"),
    });
    (uniform, buffer, bind_group, layout)
}

fn mk_render_pipeline(
    device: &wgpu::Device,
    texture_bind_group_layout: &wgpu::BindGroupLayout,
    camera_bind_group_layout: &wgpu::BindGroupLayout,
    format: wgpu::TextureFormat,
) -> wgpu::RenderPipeline {
    let shader = device.create_shader_module(wgpu::include_wgsl!("shader.wgsl"));
    let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: Some("Render Pipeline Layout"),
        bind_group_layouts: &[texture_bind_group_layout, camera_bind_group_layout],
        push_constant_ranges: &[],
    });

    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(&layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[model::VERTEX_LAYOUT, model::INSTANCE_LAYOUT],
        },
        // fragment is technically optional so wrapped in Some()
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            // "targets" is telling wgpu what color outputs it should set up
            targets: &[Some(wgpu::ColorTargetState {
                format,
                blend: Some(wgpu::BlendState::REPLACE),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
            polygon_mode: wgpu::PolygonMode::Fill,
            // Requires Features::DEPTH_CLIP_CONTROL
            unclipped_depth: false,
            // Requires Features::CONSERVATIVE_RASTERIZATION
            conservative: false,
        },
        // aka depth buffer
        depth_stencil: Some(wgpu::DepthStencilState {
            format: texture::DEPTH_FORMAT,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None, // for array textures
    })
}

fn mk_texture_bindings(
    device: &wgpu::Device,
    diffuse_texture: &texture::Texture,
) -> (wgpu::BindGroup, wgpu::BindGroupLayout) {
    let layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                // This should match the filterable field of the
                // corresponding Texture entry above.
                ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                count: None,
            },
        ],
        label: Some("texture_bind_group_layout"),
    });
    let group = device.create_bind_group(&wgpu::BindGroupDescriptor {
        layout: &layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
            },
        ],
        label: Some("diffuse_bind_group"),
    });

    (group, layout)
}
